# Ubuntu/Debian package for Zimbra backend for Z-Push

## Sources update

Everything from the release tgz must be copied in the `src/usr/share/z-push/backend/zimbra` directory, except for the `config.php` file which must be
placed in `src/etc/z-push` and renamed to `zimbra.conf.php`.

## Package creation

Create a changelog entry with
```
dch -D xenial
```
Check that author line has the correct name and email.


To TEST build run:
```
dpkg-buildpackage -us -uc
```
To build and sign run:
```
dpkg-buildpackage -S
```

To upload:
```
dput ppa:lorenzo-milesi/zimbrabackend-zpush PATH_TO_CHANGES_FILE
```

Other distros can be made availabe by [copying](https://help.launchpad.net/Packaging/PPA/Copying) the package,
once it has been published to the PPA.

## Env preparation (Docker)

```
docker run -d -it --name build -v ~/.gnupg:/root/.gnupg -v ~/.ssh:/root/.ssh -v ~/work/zimbrabackend-package:/usr/src/zimbrabackend-package ubuntu:xenial
```
Enter the container with
```
docker exec -it build /bin/bash
```
and inside that:
```
apt update && apt -y install tmux git build-essential vim subversion devscripts debhelper
```

